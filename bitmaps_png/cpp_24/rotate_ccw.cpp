
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48, 0x59, 0x73, 0x00, 0x00, 0x0e, 0xc3, 0x00, 0x00, 0x0e,
 0xc3, 0x01, 0xc7, 0x6f, 0xa8, 0x64, 0x00, 0x00, 0x00, 0x19, 0x74, 0x45, 0x58, 0x74, 0x53, 0x6f,
 0x66, 0x74, 0x77, 0x61, 0x72, 0x65, 0x00, 0x77, 0x77, 0x77, 0x2e, 0x69, 0x6e, 0x6b, 0x73, 0x63,
 0x61, 0x70, 0x65, 0x2e, 0x6f, 0x72, 0x67, 0x9b, 0xee, 0x3c, 0x1a, 0x00, 0x00, 0x00, 0x0e, 0x74,
 0x45, 0x58, 0x74, 0x54, 0x69, 0x74, 0x6c, 0x65, 0x00, 0x6d, 0x69, 0x72, 0x72, 0x6f, 0x72, 0x5f,
 0x68, 0x1c, 0x96, 0x58, 0x4c, 0x00, 0x00, 0x00, 0x52, 0x74, 0x45, 0x58, 0x74, 0x43, 0x6f, 0x70,
 0x79, 0x72, 0x69, 0x67, 0x68, 0x74, 0x00, 0x43, 0x43, 0x20, 0x41, 0x74, 0x74, 0x72, 0x69, 0x62,
 0x75, 0x74, 0x69, 0x6f, 0x6e, 0x2d, 0x53, 0x68, 0x61, 0x72, 0x65, 0x41, 0x6c, 0x69, 0x6b, 0x65,
 0x20, 0x68, 0x74, 0x74, 0x70, 0x3a, 0x2f, 0x2f, 0x63, 0x72, 0x65, 0x61, 0x74, 0x69, 0x76, 0x65,
 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x73, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x6c, 0x69, 0x63, 0x65,
 0x6e, 0x73, 0x65, 0x73, 0x2f, 0x62, 0x79, 0x2d, 0x73, 0x61, 0x2f, 0x34, 0x2e, 0x30, 0x2f, 0xc3,
 0x54, 0x62, 0x05, 0x00, 0x00, 0x01, 0xee, 0x49, 0x44, 0x41, 0x54, 0x48, 0x89, 0xed, 0x93, 0xcf,
 0x6b, 0x53, 0x41, 0x10, 0x80, 0xbf, 0x79, 0x2f, 0x29, 0xa1, 0xb5, 0x20, 0x12, 0xc5, 0x5a, 0xf1,
 0xe7, 0x45, 0x28, 0xde, 0x04, 0xa5, 0xa9, 0x20, 0x5e, 0x44, 0xb0, 0xf8, 0x17, 0x08, 0xa2, 0x60,
 0x4f, 0x5e, 0x84, 0x60, 0x48, 0x02, 0x7d, 0x49, 0xd6, 0x98, 0x2a, 0x22, 0x28, 0x1e, 0x44, 0xbd,
 0x78, 0x51, 0xaa, 0x78, 0x11, 0x15, 0x4f, 0x15, 0x79, 0xda, 0x83, 0xa8, 0x48, 0xe3, 0x49, 0x41,
 0x14, 0x5b, 0x10, 0x5a, 0x45, 0x25, 0x6a, 0x49, 0xb2, 0xe3, 0xa1, 0xbc, 0x62, 0x21, 0xbc, 0x34,
 0xc1, 0x9c, 0xf4, 0x3b, 0xcd, 0xee, 0xce, 0xce, 0x37, 0xb3, 0xb0, 0xf0, 0x9f, 0x26, 0xc8, 0x72,
 0x92, 0xd6, 0x7b, 0x4f, 0x57, 0x59, 0xb1, 0x0f, 0x05, 0xa9, 0x4c, 0x8f, 0x26, 0xf6, 0xb4, 0x22,
 0x70, 0x9a, 0x25, 0xac, 0xf6, 0x26, 0x56, 0x58, 0xb1, 0xf7, 0x81, 0x1d, 0xa0, 0xdd, 0xad, 0x14,
 0x6f, 0x2a, 0x18, 0xf0, 0x5e, 0x77, 0x45, 0x25, 0x72, 0x1b, 0xd8, 0x29, 0x30, 0x53, 0xc7, 0xe6,
 0x36, 0x9e, 0x7a, 0xdc, 0xd7, 0x8a, 0x20, 0xf4, 0x89, 0xfa, 0xbd, 0x27, 0x97, 0x55, 0xf4, 0x18,
 0x22, 0x8a, 0xea, 0x9f, 0xb9, 0xef, 0x14, 0xae, 0xd8, 0xee, 0xca, 0x85, 0x4f, 0xc9, 0x7d, 0x95,
 0xb0, 0x1a, 0xa1, 0x13, 0xa8, 0xa8, 0xbb, 0x10, 0xa8, 0x15, 0xf4, 0x0d, 0xc8, 0x0b, 0x60, 0x0e,
 0xd8, 0x2c, 0x50, 0x74, 0x7f, 0xf4, 0x4c, 0xae, 0xf5, 0x26, 0x37, 0xb5, 0x2d, 0x70, 0x7a, 0x9d,
 0xe3, 0x2a, 0x3c, 0x02, 0x5c, 0x45, 0x1c, 0x57, 0x75, 0x78, 0x46, 0x13, 0x6b, 0x04, 0xf6, 0x0a,
 0x94, 0x81, 0xed, 0x8e, 0xd4, 0x1f, 0xc4, 0xc7, 0xfc, 0xde, 0xb6, 0x04, 0x1f, 0x4f, 0x0c, 0xfe,
 0x9c, 0xb7, 0x91, 0x83, 0x02, 0xcf, 0x80, 0xad, 0x75, 0xe1, 0x0e, 0x9e, 0xd8, 0xe9, 0xd1, 0xa1,
 0x89, 0xf9, 0x18, 0x83, 0xc0, 0x2b, 0x60, 0x5b, 0xd7, 0x2f, 0xc9, 0xb4, 0x25, 0x00, 0xf8, 0xec,
 0xed, 0xfa, 0x46, 0x34, 0xba, 0x1f, 0x78, 0x09, 0x54, 0x83, 0xfd, 0xd9, 0x93, 0x43, 0xdf, 0x55,
 0xf4, 0xe8, 0xc2, 0x4a, 0x8f, 0x30, 0x3e, 0xee, 0x86, 0x16, 0x2a, 0x95, 0x4a, 0x1b, 0x9a, 0xc9,
 0x1a, 0xd1, 0x9f, 0xf3, 0xa7, 0xd6, 0xe5, 0x7c, 0xed, 0xcb, 0xfb, 0xbb, 0x1b, 0x9d, 0x2f, 0x4e,
 0x50, 0xab, 0xd5, 0xde, 0x1a, 0x63, 0xae, 0xe7, 0xf3, 0xf9, 0x81, 0x56, 0x04, 0x0a, 0xcf, 0x01,
 0x1c, 0x65, 0x4b, 0xa8, 0x00, 0x88, 0x02, 0x87, 0x1c, 0xc7, 0x99, 0x32, 0xc6, 0xdc, 0x2d, 0x16,
 0x8b, 0x89, 0xe5, 0x08, 0x44, 0xf9, 0x02, 0x60, 0x45, 0xea, 0xcd, 0x04, 0x8b, 0x77, 0x80, 0x03,
 0xd6, 0x5a, 0xbf, 0x50, 0x28, 0xf8, 0xc6, 0x98, 0x61, 0x5d, 0xfa, 0x07, 0x96, 0x50, 0x43, 0x4e,
 0x0b, 0x1c, 0xee, 0x59, 0xf9, 0xf5, 0x56, 0xc3, 0x06, 0x82, 0xc0, 0x18, 0xa3, 0x21, 0x8d, 0x96,
 0x45, 0xe4, 0x6c, 0x3c, 0x1e, 0xbf, 0x31, 0x32, 0x32, 0x52, 0x0d, 0xc9, 0x6b, 0x5b, 0x10, 0xf0,
 0x5e, 0x55, 0xcf, 0xc7, 0x62, 0xb1, 0xab, 0xc9, 0x64, 0x32, 0xf4, 0x07, 0xb7, 0x2b, 0x08, 0x98,
 0x55, 0xd5, 0x4b, 0xae, 0xeb, 0x5e, 0x4c, 0xa7, 0xd3, 0x73, 0x9d, 0x10, 0x04, 0x54, 0x80, 0x6b,
 0x91, 0x48, 0xe4, 0x5c, 0x2a, 0x95, 0xfa, 0xd0, 0x09, 0x41, 0x40, 0x15, 0xb8, 0x29, 0x22, 0x67,
 0x32, 0x99, 0x4c, 0xb9, 0x13, 0x82, 0x00, 0x05, 0xee, 0x01, 0x63, 0xd9, 0x6c, 0xd6, 0xff, 0x0b,
 0xf5, 0xfe, 0x05, 0x7e, 0x03, 0xee, 0x44, 0xb1, 0x50, 0xc3, 0xe5, 0x10, 0x4d, 0x00, 0x00, 0x00,
 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE rotate_ccw_xpm[1] = {{ png, sizeof( png ), "rotate_ccw_xpm" }};

//EOF
